package totabraz.com.centeramigo.activities;

import android.location.Address;
import android.location.Geocoder;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import com.location.aravind.getlocation.GeoLocator;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import totabraz.com.centeramigo.R;

public class SolicitarCaronaActivity extends AppCompatActivity {

    private TextInputEditText tiet_city;
    private TextInputEditText tiet_state;
    private TextInputEditText tiet_zip;
    private TextInputEditText tiet_country;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitar_carona);
        getSupportActionBar().hide();

        tiet_city = findViewById(R.id.tiet_city);
        tiet_state = findViewById(R.id.tiet_state);
        tiet_zip = findViewById(R.id.tiet_zip);
        tiet_country = findViewById(R.id.tiet_country);

        GeoLocator geoLocator = new GeoLocator(getApplicationContext(),SolicitarCaronaActivity.this);
        getAndCompleteAddressForm(geoLocator.getLattitude(),geoLocator.getLongitude());
    }

    private void getAndCompleteAddressForm(double LATITUDE, double LONGITUDE) {
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(LATITUDE,LONGITUDE, 1);
            String city = addresses.get(0).getLocality();
            tiet_city.setText(city);
            String state = addresses.get(0).getAdminArea();
            tiet_state.setText(state);
            String zip = addresses.get(0).getPostalCode();
            tiet_zip.setText(zip);
            String country = addresses.get(0).getCountryName();
            tiet_country.setText(country);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
