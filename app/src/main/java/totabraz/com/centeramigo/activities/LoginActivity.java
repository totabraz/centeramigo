package totabraz.com.centeramigo.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

import totabraz.com.centeramigo.R;

public class LoginActivity extends AppCompatActivity {

    private final String TAG = "Firebase";
    private FirebaseAuth mAuth;
    private TextInputEditText edEmail;
    private TextInputEditText edPasswd;
    private LinearLayout llLoginArea;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        this.progressBar = findViewById(R.id.progressBar);
        this.mAuth = FirebaseAuth.getInstance();
        this.edEmail = findViewById(R.id.edtEmail);
        edEmail.setText("tota.braz@gmail.com");
        this.edPasswd = findViewById(R.id.edtPasswd);
        edPasswd.setText("123123123");
        this.llLoginArea = findViewById(R.id.llLoginArea);
        final Button btnLogin = findViewById(R.id.btnSingIn);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        this.edPasswd.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            login();
                            hideKeyboard(edPasswd);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
    }


    private void login() {
        if ((edEmail.getText().toString().length() > 0) && (edPasswd.getText().toString().length() > 0)) {
            this.progressBar.setVisibility(View.VISIBLE);
            this.llLoginArea.setVisibility(View.GONE);
            String mail = edEmail.getText().toString();
            String pswd = edPasswd.getText().toString();
            setupFirebaseUser(mail, pswd);
        }
    }

    private void setupFirebaseUser(String mail, String pswd) {
        this.mAuth.signInWithEmailAndPassword(mail, pswd)
            .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    } else {
                        Toast.makeText(getApplicationContext(), "Erro ao Entrar", Toast.LENGTH_LONG).show();
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                    }
                }
            });
        progressBar.setVisibility(View.GONE);
        llLoginArea.setVisibility(View.VISIBLE);

    }

    public void singUp() {
        String email = Objects.requireNonNull(edEmail.getText()).toString();
        String pswd = Objects.requireNonNull(edPasswd.getText()).toString();
        if ((email.length() > 0) && (pswd.length() > 0)) {
            this.mAuth.createUserWithEmailAndPassword(email, pswd)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Erro ao Cadastrar", Toast.LENGTH_LONG).show();
                        }
                    }
                });
        }

    }


    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}