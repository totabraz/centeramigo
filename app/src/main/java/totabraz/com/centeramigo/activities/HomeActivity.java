package totabraz.com.centeramigo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

import totabraz.com.centeramigo.R;

public class HomeActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private Button btnPedirCarona;
    private Button btnDarCarona;
    private Button btnSingIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();

        mAuth = FirebaseAuth.getInstance();

        btnPedirCarona = findViewById(R.id.btnPedirCarona);
        btnPedirCarona.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), SolicitarCaronaActivity.class)));

        btnDarCarona = findViewById(R.id.btnDarCarona);
        btnDarCarona.setOnClickListener(v -> {

        });

        btnSingIn = findViewById(R.id.btnSingIn);
        btnSingIn.setOnClickListener(v -> {

        });

    }
}
